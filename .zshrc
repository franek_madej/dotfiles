# sometimes it's not present (?)
export LC_ALL=en_US.UTF-8
export LANG=en_US

# check if OS is macOS, Android or Linux
if [ "$(uname -s)" = "Darwin" ]; then
    export MY_OS_TYPE="macOS"
    export XDG_CONFIG_HOME="$HOME/.config"
elif [ "$(uname -o)" = "Android" ]; then
    export MY_OS_TYPE="Android"
else
    export MY_OS_TYPE="Linux"
fi

# When on OS X
if [ "$MY_OS_TYPE" = "macOS" ]; then
    PATH="/opt/homebrew/bin:$PATH"
    PATH="/opt/homebrew/sbin:$PATH"

    # ruby
    PATH="/opt/homebrew/opt/ruby/bin:$PATH"
    PATH="/opt/homebrew/lib/ruby/gems/3.0.0/bin:$PATH"
fi

# Neovim if possible
if which nvim 1> /dev/null 2>&1; then
    EDITOR=nvim
    alias vim=nvim
else
    EDITOR=vim
fi
VISUAL=$EDITOR
export $EDITOR
export $VISUAL

alias n=nvim

# configuration files editing
alias zshrc="$EDITOR ~/.zshrc"
alias vimrc="$EDITOR ~/.vimrc"
alias nvimrc="$EDITOR ~/.config/nvim/init.lua"

# prompt
eval "$(starship init zsh)"

# zoxide (directory jumping)
eval "$(zoxide init zsh)"

# preview window via eza, has to be defined before oh-my-zsh/fzf plugin
export FZFZ_PREVIEW_COMMAND="eza --git -h -l --color=always {}"

# Plugin manager for Zsh
source "${HOME}/.zgenom/zgenom.zsh"

# disable auto update prompt for omz
DISABLE_AUTO_UPDATE=true

# Check if there's no init script
if ! zgenom saved; then
    echo "Creating a zgenom save"

    zgenom load zsh-users/zsh-autosuggestions
    zgenom load zsh-users/zsh-completions src

    # oh-my-zsh plugins
    zgenom oh-my-zsh

    zgenom oh-my-zsh plugins/vi-mode
    zgenom oh-my-zsh plugins/fzf
    zgenom oh-my-zsh plugins/git
    zgenom oh-my-zsh plugins/pass
    zgenom oh-my-zsh plugins/asdf
    zgenom oh-my-zsh plugins/systemd
    zgenom load chrissicool/zsh-256color
    
    zgenom load zdharma-continuum/fast-syntax-highlighting
    # save all to init script
    zgenom save
fi

export ANDROID_HOME=$HOME/projects/.android
if [ "$MY_OS_TYPE" = "macOS" ]; then
  export ANDROID_HOME=/Users/kosciak/Library/Android/sdk
  export JAVA_HOME=/Applications/Android\ Studio.app/Contents/jbr/Contents/Home/
fi
export ANDROID_SDK_ROOT=$ANDROID_HOME
PATH=${PATH}:$ANDROID_HOME/tools/bin:$ANDROID_HOME/platform-tools:$ANDROID_HOME/emulator

# Some useful scripts I've picked up along the way
PATH="$HOME/projects/.bin:$PATH"

# Some useful shortcuts
alias sc-suspend="sudo systemctl suspend" # why was it missing!
alias cp="cp -rv --reflink=auto" # btrfs!
alias l="eza --git -h -g -H -l"

# force trash-cli on linux
if [ "$MY_OS_TYPE" = "Linux" ]; then
  alias rm='echo "This is not the command you are looking for."; false' # trash-cli!
  alias sudo='sudo ' # rm fix (so you can't sudo rm files)
fi

# zk
export ZK_NOTEBOOK_DIR="$HOME/notes"

## local environments 

### ruby
if which rbenv 1> /dev/null 2>&1; then
  eval "$(rbenv init -)"
fi

### python
if which pyenv 1> /dev/null 2>&1; then
  export PYENV_ROOT="$HOME/.pyenv"
  PATH="$PYENV_ROOT/bin:$PATH"
  eval "$(pyenv init --path)"
  eval "$(pyenv init -)"
fi

### nodejs
export VOLTA_FEATURE_PNPM=1
export VOLTA_HOME="$HOME/.volta"
PATH="$VOLTA_HOME/bin:$PATH"

### locally installed stuff
PATH="$HOME/.local/bin:$PATH"
PATH="$HOME/.cargo/bin:$PATH"

# BEGIN_KITTY_SHELL_INTEGRATION
if test -e "/usr/lib/kitty/shell-integration/kitty.zsh"; then source "/usr/lib/kitty/shell-integration/kitty.zsh"; fi
# END_KITTY_SHELL_INTEGRATION
