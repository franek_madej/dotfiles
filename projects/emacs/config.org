#+TITLE: emacs
#+PROPERTY: header-args:emacs-lisp :tangle ~/.emacs.d/init.el :mkdirp yes

* sanemacs
#+begin_src emacs-lisp
  (load "~/.emacs.d/sanemacs.el" nil t)
  (setq package-enable-at-startup nil)
#+end_src

* straight package manager
#+begin_src emacs-lisp
  (defvar bootstrap-version)

  ;; for emacs 29.1
  (setq straight-repository-branch "develop")

  (let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 6))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
    	(url-retrieve-synchronously
    	 "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
    	 'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))

  (straight-use-package 'use-package)
  (setq straight-use-package-by-default t)
#+end_src

* evil mode
#+begin_src emacs-lisp
  (use-package evil
    :ensure t
    :config
    (evil-mode 1))

  (use-package org
    :after geiser-guile
    :config
    (require 'org-tempo)
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((scheme . t)))
    )
#+end_src

* modeline
#+begin_src emacs-lisp
  (use-package doom-modeline
    :config
    (setq doom-modeline-buffer-encoding nil)
    (setq doom-modeline-modal-icon nil)
    :init
    (doom-modeline-mode 1))
			      #+end_src

* fonts
#+begin_src emacs-lisp
  (defvar k9/default-font-size 200)
  (defvar k9/default-variable-font-size 200)

  (set-face-attribute 'default nil :font "Iosevka Nerd Font Mono" :height k9/default-font-size)
  ;; Set the fixed pitch face
  (set-face-attribute 'fixed-pitch nil :font "Iosevka Nerd Font Mono" :height k9/default-font-size)
  ;; Set the variable pitch face
  (set-face-attribute 'variable-pitch nil :font "Noto Sans" :height k9/default-font-size :weight 'regular)
#+end_src

* kanagawa color scheme
#+begin_src emacs-lisp
  (use-package autothemer
    :config
    (load "~/.emacs.d/kanagawa-theme.el" nil t)
    (load-theme 'kanagawa))
#+end_src

* scheme
#+begin_src emacs-lisp
  (use-package geiser-guile)
  (use-package paredit
    :init
    (add-hook 'clojure-mode-hook #'enable-paredit-mode)
    (add-hook 'cider-repl-mode-hook #'enable-paredit-mode)
    (add-hook 'emacs-lisp-mode-hook #'enable-paredit-mode)
    (add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
    (add-hook 'ielm-mode-hook #'enable-paredit-mode)
    (add-hook 'lisp-mode-hook #'enable-paredit-mode)
    (add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
    (add-hook 'scheme-mode-hook #'enable-paredit-mode)
    :config
    (show-paren-mode t)
    :bind (("M-[" . paredit-wrap-square)
  	 ("M-{" . paredit-wrap-curly))
    :diminish nil)
  (use-package parinfer-rust-mode
    :init
    (setq parinfer-rust-auto-download t)
    :hook (scheme-mode emacs-lisp-mode))

#+end_src

#+RESULTS:

* org mode

one file with TODOs

#+begin_src emacs-lisp
  (defun dw/org-mode-setup ()
    (org-indent-mode)
    (variable-pitch-mode 1)
    (auto-fill-mode 0)
    (visual-line-mode 1)
    (setq evil-auto-indent nil))

  (use-package org
    :hook (org-mode . dw/org-mode-setup)
    :config
    (setq org-ellipsis " ▾"
          org-hide-emphasis-markers t))

    (use-package org-bullets
      :after org
      :hook (org-mode . org-bullets-mode)
      :custom
      (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

    ;; Replace list hyphen with dot
    (font-lock-add-keywords 'org-mode
                            '(("^ *\\([-]\\) "
                              (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

    (dolist (face '((org-level-1 . 1.2)
                    (org-level-2 . 1.1)
                    (org-level-3 . 1.05)
                    (org-level-4 . 1.0)
                    (org-level-5 . 1.1)
                    (org-level-6 . 1.1)
                    (org-level-7 . 1.1)
                    (org-level-8 . 1.1)))
        (set-face-attribute (car face) nil :font "Helvetica" :weight 'regular :height (cdr face)))

    ;; Make sure org-indent face is available
    (require 'org-indent)

    ;; Ensure that anything that should be fixed-pitch in Org files appears that way
    (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
    (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
    (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)
#+end_src

#+RESULTS:
