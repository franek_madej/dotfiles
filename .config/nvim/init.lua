vim.g.loaded = 1
vim.g.loaded_netrwPlugin = 1
-- use lua filetype detection
vim.g.do_filetype_lua = 1

-- nvim options setup
local o = vim.o

o.encoding = "utf-8"   -- https://xkcd.com/927/
o.autoindent = true    -- Indent according to previous line.
o.expandtab = true     -- Use spaces instead of tabs
o.tabstop = 2          -- Tab key indents by 4 spaces
o.shiftwidth = 2       -- >> indents by tabstop spaces
o.softtabstop = 4      -- >> indents by softtabstop spaces
o.shiftround = true    -- >> indents to next multiple of 'shiftwidth'

o.textwidth = 79       -- terminals
o.signcolumn = "yes"   -- don't "jump" the UI with linters
o.display = "lastline" -- show as much as possible of the last line
o.hidden = true        -- switch between buffers without having to save
o.showcmd = true       -- show already typed keys when more are expected
o.showmode = false     -- hide "-INSERT–"

o.incsearch = true     -- highlight search results
o.hlsearch = true      -- highlight _all_ search results
o.ignorecase = true    -- ignore case when searching
o.scrolloff = 12       -- cursor offset on searching
o.wrapscan = true      -- wrap search on end

o.laststatus = 2       -- always show statusline

vim.o.foldmethod = "expr"
vim.o.foldexpr = "v:lua.vim.treesitter.foldexpr()"
vim.o.foldcolumn = "0"
vim.o.foldlevel = 99
vim.o.foldlevelstart = 99
vim.o.foldenable = true

o.swapfile = true           -- use swap files
o.dir = "/tmp"              -- keep swap files in tmp
o.smartcase = true          -- ignore case only when there is no uppercase chars

o.cursorline = true         -- find the current line quickly.
o.number = true             -- show line numbers
o.relativenumber = true     -- ...but relative ones

o.termguicolors = true      -- better term GUI

o.splitkeep = "screen"      -- keep buffers stable on opening another split (stop UI jumps)

o.clipboard = "unnamedplus" -- copy to system clipboard

-- use space for leader key
vim.keymap.set("n", " ", "<Nop>", { silent = true, remap = false })
vim.g.mapleader = " "

-- unclutter feedback
o.shortmess = "lmrwaIFScCWT"

-- don't show ~ on empty lines
vim.wo.fillchars = "eob: "

-- quick update for treesitter comments
vim.opt.updatetime = 100
-- speed up loading for comment resolving
vim.g.skip_ts_context_commentstring_module = true

vim.g.codeium_disable_bindings = 1

-- install lazy.nvim if not present
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

-- it's actually defined, something is off with LSP
---@diagnostic disable-next-line: undefined-field
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  {
    "echasnovski/mini.nvim",
    config = function()
      require("mini.sessions").setup()
      require("mini.starter").setup({
        header = "",
        footer = "",
      })
    end,
  },
  {
    "rebelot/kanagawa.nvim",
    config = function()
      require("kanagawa").setup({
        compile = true,
        dimInactive = true,
        theme = "wave",
        overrides = function(colors)
          local theme = colors.theme
          return {
            -- borderless telescope
            TelescopeTitle = { fg = theme.ui.special, bold = true },
            TelescopePromptNormal = { bg = theme.ui.bg_p1 },
            TelescopePromptBorder = {
              fg = theme.ui.bg_p1,
              bg = theme.ui.bg_p1,
            },
            TelescopeResultsNormal = {
              fg = theme.ui.fg_dim,
              bg = theme.ui.bg_m1,
            },
            TelescopeResultsBorder = {
              fg = theme.ui.bg_m1,
              bg = theme.ui.bg_m1,
            },
            TelescopePreviewNormal = { bg = theme.ui.bg_dim },
            TelescopePreviewBorder = {
              bg = theme.ui.bg_dim,
              fg = theme.ui.bg_dim,
            },

            -- more uniform colors for popup menu
            Pmenu = { fg = theme.ui.shade0, bg = theme.ui.bg_p1 },
            PmenuSel = { fg = "NONE", bg = theme.ui.bg_p2 },
            PmenuSbar = { bg = theme.ui.bg_m1 },
            PmenuThumb = { bg = theme.ui.bg_p2 },
          }
        end,
      })

      vim.cmd("colorscheme kanagawa")
    end,
  },

  -- tips to follow best practices
  {
    "m4xshen/hardtime.nvim",
    dependencies = { "MunifTanjim/nui.nvim" },
    opts = {},
  },

  -- uniform icons (always circle)
  {
    "projekt0n/circles.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
      require("circles").setup({
        lsp = true,
      })
    end,
  },

  -- auto-resize splits
  {
    "nvim-focus/focus.nvim",
    version = "*",
    config = function()
      -- disable focus auto-resize for NvimTree and Trouble
      vim.api.nvim_create_autocmd("FileType", {
        group = vim.api.nvim_create_augroup("FocusDisable", { clear = true }),
        callback = function(_)
          if vim.tbl_contains({ "NvimTree", "trouble" }, vim.bo.filetype) then
            vim.b.focus_disable = true
          else
            vim.b.focus_disable = false
          end
        end,
        desc = "Disable focus autoresize for FileType",
      })

      require("focus").setup({
        ui = {
          cursorline = false,
          signcolumn = false,
        },
      })
    end,
  },

  -- statusline
  {
    "rebelot/heirline.nvim",
    dependencies = {
      "rebelot/kanagawa.nvim",
    },
    config = function()
      require("ui.statusline")
    end,
  },

  -- git change status in gutter
  {
    "lewis6991/gitsigns.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
    },
    config = function()
      require("gitsigns").setup()
      require("scrollbar.handlers.gitsigns").setup()
    end,
  },

  -- TODO: setup and test properly
  -- git merge resolution from within vim
  { "sindrets/diffview.nvim" },

  { "tpope/vim-fugitive" },

  {
    "isak102/telescope-git-file-history.nvim",
    dependencies = { "tpope/vim-fugitive", "nvim-telescope/telescope.nvim" },
    config = function()
      require("telescope").load_extension("git_file_history")
    end,
  },

  -- highlight undo operation result
  {
    "tzachar/highlight-undo.nvim",
    config = function()
      require("highlight-undo").setup({
        duration = 300,
        undo = {
          hlgroup = "HighlightUndo",
          mode = "n",
          lhs = "u",
          map = "undo",
          opts = {},
        },
        redo = {
          hlgroup = "HighlightUndo",
          mode = "n",
          lhs = "<C-r>",
          map = "redo",
          opts = {},
        },
      })
    end,
  },

  -- better folding
  {
    "kevinhwang91/nvim-ufo",
    dependencies = "kevinhwang91/promise-async",
    config = function()
      require("ufo").setup({
        provider_selector = function()
          return { "treesitter", "indent" }
        end,
      })
    end,
  },

  -- move by words (camel case)
  { "chaoren/vim-wordmotion" },

  -- better commenting
  {
    "numToStr/Comment.nvim",
    dependencies = {
      "JoosepAlviste/nvim-ts-context-commentstring",
    },
    config = function()
      require("Comment").setup({
        pre_hook = function()
          return vim.bo.commentstring
        end,
      })
    end,
  },

  -- auto-close keywords (def end in python)
  {
    "RRethy/nvim-treesitter-endwise",
    config = function()
      require("nvim-treesitter.configs").setup({
        endwise = {
          enable = true,
        },
      })
    end,
  },

  -- auto-close brackets, quotes, etc
  {
    "windwp/nvim-autopairs",
    config = function()
      require("nvim-autopairs").setup()
    end,
  },

  -- colored pairs of brackets
  {
    "hiphish/rainbow-delimiters.nvim",
    config = function()
      local rainbow_delimiters = require("rainbow-delimiters")
      vim.g.rainbow_delimiters = {
        strategy = {
          [""] = rainbow_delimiters.strategy["global"],
          vim = rainbow_delimiters.strategy["local"],
        },
        query = {
          [""] = "rainbow-delimiters",
          lua = "rainbow-blocks",
          tsx = "rainbow-tags-react",
          javascript = "rainbow-tags-react",
        },
        highlight = {
          "RainbowDelimiterRed",
          "RainbowDelimiterYellow",
          "RainbowDelimiterBlue",
          "RainbowDelimiterOrange",
          "RainbowDelimiterGreen",
          "RainbowDelimiterViolet",
          "RainbowDelimiterCyan",
        },
      }
    end,
  },

  {
    "kylechui/nvim-surround",
    event = "VeryLazy",
    config = function()
      require("nvim-surround").setup({})
    end,
  },

  {
    "williamboman/mason.nvim",
    opts = {},
  },

  -- auto-install LSP tools
  {
    "WhoIsSethDaniel/mason-tool-installer.nvim",
    dependencies = { "williamboman/mason.nvim" },
    config = function()
      require("mason-tool-installer").setup({
        ensure_installed = {
          -- formatters
          "ruff",
          -- python
          "prettierd",
          -- javascript

          -- nvim configuration mostly :)
          "stylua",

          -- 'prose'
          "markdownlint",
        },

        auto_update = true,
        run_on_start = true,
        start_delay = 3000,
        debounce_hours = 5,
      })
    end,
  },

  -- Terraform support
  { "hashivim/vim-terraform" },

  -- lsp setup
  {
    "williamboman/mason-lspconfig.nvim",
    dependencies = {
      "williamboman/mason.nvim",
      "neovim/nvim-lspconfig",
    },
    opts = {
      -- Replace the language servers listed here
      -- with the ones you want to install
      ensure_installed = {
        "elixirls",
        "eslint",
        "yamlls",
        "tailwindcss",
        "harper_ls",
        "lua_ls",
        "ts_ls",
      },
      handlers = {
        harper_ls = function()
          require("lspconfig").harper_ls.setup({
            filetypes = { "elixir", "eelixir" },
          })
        end,
        function(server_name)
          require("lspconfig")[server_name].setup({})
        end,
      },
    },
  },

  -- typescript IDE
  {
    "pmizio/typescript-tools.nvim",
    dependencies = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
    opts = {},
  },

  -- auto-completion
  {
    "saghen/blink.cmp",
    version = "v0.*",
    lazy = false, -- lazy loading handled internally
    opts = {
      keymap = { preset = "default", ["<C-i>"] = { "show", "show_documentation" } },
      signature = { enabled = true },
    },
  },
  {
    "stevearc/conform.nvim",
    event = { "BufWritePre" },
    cmd = { "ConformInfo" },
    opts = {
      formatters_by_ft = {
        lua = { "stylua" },
        python = { "ruff_format", "isort", "black", stop_after_first = true },
        javascript = { "prettierd", "prettier", stop_after_first = true },
        typescript = { "prettierd", "prettier", stop_after_first = true },
        typescriptreact = { "prettierd", "prettier", stop_after_first = true },
        graphql = { "prettierd", "prettier", stop_after_first = true },
        json = { "prettierd", "prettier", stop_after_first = true },
        markdown = { "markdownlint", "prettierd", "prettier", stop_after_first = true },
      },

      default_format_opts = {
        lsp_format = "fallback",
      },

      format_on_save = { timeout_ms = 500, lsp_fallback = true },
    },
    init = function()
      -- If you want the formatexpr, here is the place to set it
      vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"
    end,
  },

  {
    "rachartier/tiny-inline-diagnostic.nvim",
    enabled = false,
    event = "VeryLazy",
    config = function()
      require("tiny-inline-diagnostic").setup()
    end,
  },

  -- LSP diagnostics in bottom tray
  {
    "folke/trouble.nvim",
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    },
    cmd = "Trouble",
    opts = {
      open_no_results = true,
      icons = {
        indent = {
          middle = " ",
          last = " ",
          top = " ",
          ws = "│  ",
        },
      },
      modes = {
        diagnostics = {
          groups = {
            { "filename", format = "{file_icon} {basename:Title} {count}" },
          },
        },
      },
    },
  },

  {
    "OXY2DEV/helpview.nvim",
    ft = "help",
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
    },
  },

  {
    "Exafunction/codeium.vim",
    event = "BufEnter",
    config = function()
      vim.g.codeium_filetypes = {
        ["markdown"] = false,
      }
    end,
  },

  -- TreeSitter for better syntax highlighting
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
      require("nvim-treesitter.configs").setup({
        highlight = {
          enable = true,
          -- disable TS if file is big (makes nvim usable then)
          disable = function(_, buf)
            local max_filesize = 100 * 1024 -- 100 KB
            local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
            if ok and stats and stats.size > max_filesize then
              return true
            end
          end,
          additional_vim_regex_highlighting = false,
        },
        indent = {
          enable = true,
        },
        ensure_installed = {
          "markdown",
          "markdown_inline",
          "tsx",
          "javascript",
          "typescript",
          "json",
          "graphql",
          "python",
          "yaml",
          "html",
          "scss",
          "lua",
          "elixir",
          "heex",
        },
      })
      local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
      parser_config.tsx.filetype_to_parsername = { "javascript", "typescript.tsx" }
    end,
  },

  -- fuzzy search for almost everything
  {
    "nvim-telescope/telescope.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
    },
  },

  -- use fzf for telescope
  {
    "nvim-telescope/telescope-fzf-native.nvim",
    dependencies = {
      "nvim-telescope/telescope.nvim",
    },
    build =
    "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build",
    config = function()
      require("telescope").load_extension("fzf")
    end,
  },

  -- add projects support to telescope
  {
    "nvim-telescope/telescope-project.nvim",
    dependencies = {
      "nvim-telescope/telescope.nvim",
    },
    config = function()
      require("telescope").load_extension("project")
    end,
  },

  -- automatic buffer resizing
  {
    "kwkarlwang/bufresize.nvim",
    config = function()
      require("bufresize").setup()
    end,
  },

  -- smarter navigation / resizing for buffers
  {
    "mrjones2014/smart-splits.nvim",
    config = function()
      require("smart-splits").setup({
        resize_mode = {
          hooks = {
            on_leave = require("bufresize").register,
          },
        },
      })
    end,
  },
  -- manage files like it's a buffer
  {
    "stevearc/oil.nvim",
    opts = {},
    -- Optional dependencies
    dependencies = { "nvim-tree/nvim-web-devicons" },
  },

  -- better UI for :messages
  {
    "rcarriga/nvim-notify",
    config = function()
      require("notify").setup({
        stages = "fade",
        render = "compact",
      })
      vim.notify = require("notify")
    end,
  },

  -- replace builtin vim inputs / selects with nicer alternatives (e.g.
  -- telescope)
  { "stevearc/dressing.nvim" },

  -- highlight search results
  {
    "kevinhwang91/nvim-hlslens",
    config = function()
      require("scrollbar.handlers.search").setup()
    end,
  },

  -- scrollbar (like minimap in VS Code)
  {
    "petertriho/nvim-scrollbar",
    dependencies = {
      "kevinhwang91/nvim-hlslens",
      "lewis6991/gitsigns.nvim",
    },
    config = function()
      require("scrollbar").setup()
    end,
  },

  -- smooth scroll for ctrl-u / ctrl-d
  {
    "psliwka/vim-smoothie",
    enabled = vim.g.neovide == nil,
  },

  -- activity indicator for LSP
  {
    "j-hui/fidget.nvim",
    config = function()
      require("fidget").setup()
    end,
  },

  -- notes!
  {
    "mickael-menu/zk-nvim",
    config = function()
      require("zk").setup({
        picker = "telescope",
      })
    end,
  },

  -- hide UI for clutter-free experience
  {
    "folke/zen-mode.nvim",
    config = function()
      require("zen-mode").setup({
        window = {
          options = {
            signcolumn = "no",
            number = false,
            relativenumber = false,
          },
        },
      })
    end,
  },

  -- better highlighting for CSV files
  {
    "cameron-wags/rainbow_csv.nvim",
    config = function()
      require("rainbow_csv").setup()
    end,
    -- optional lazy-loading below
    module = {
      "rainbow_csv",
      "rainbow_csv.fns",
    },
    ft = {
      "csv",
      "tsv",
      "csv_semicolon",
      "csv_whitespace",
      "csv_pipe",
      "rfc_csv",
      "rfc_semicolon",
    },
  },

  -- highlight active segment
  {
    "folke/twilight.nvim",
    config = function()
      require("twilight").setup({})
    end,
  },

  -- define keymap here, with command palette
  {
    "mrjones2014/legendary.nvim",
    config = function()
      require("legendary").setup({
        default_opts = {
          keymaps = { silent = true, remap = false },
        },
        keymaps = {
          -- Legendary in Legendary
          {
            "<leader>l",
            ":Legendary<CR>",
            description = "Open command palette",
          },

          -- Codeium assistance
          {
            "<C-j>",
            function()
              return vim.fn["codeium#Accept"]()
            end,
            description = "Accept Codeium suggestion",
            mode = { "i" },
            opts = { expr = true, noremap = true },
          },
          {
            "<M-[>",
            function()
              return vim.fn["codeium#CycleCompletions"](1)
            end,
            description = "Cycle Codeium suggestion forwards",
            mode = { "i" },
            opts = { expr = true },
          },
          {
            "<M-]>",
            function()
              return vim.fn["codeium#CycleCompletions"](-1)
            end,
            description = "Cycle Codeium suggestion backwards",
            mode = { "i" },
            opts = { expr = true },
          },
          {
            "<C-x>",
            function()
              return vim.fn["codeium#Clear"]()
            end,
            description = "Clear Codeium suggestion",
            mode = { "i" },
            opts = { expr = true },
          },

          -- better built-in commands
          {
            "<leader>q",
            "<cmd>:bp<bar>sp<bar>bn<bar>bd<CR>",
            description = "Kill the buffer",
          },

          -- telescope lookups
          {
            "<leader>ff",
            ":Telescope find_files theme=dropdown<CR>",
            description = "Find files",
          },
          {
            "<leader>fp",
            "<cmd>Telescope project theme=dropdown<CR>",
            description = "Select projects",
          },
          {
            "<leader>fg",
            "<cmd>Telescope live_grep theme=dropdown<CR>",
            description = "Find word",
          },
          {
            "<leader>fh",
            "<cmd>Telescope help_tags theme=dropdown<CR>",
            description = "Find help in help_tags",
          },
          {
            "<leader>fb",
            "<cmd>Telescope buffers theme=dropdown<CR>",
            description = "Find buffers",
          },
          {
            "<leader>fn",
            "<cmd>Telescope notify theme=dropdown<CR>",
            description = "Search notifications history",
          },

          -- git
          {
            "<leader>gb",
            "<cmd>Gitsigns toggle_current_line_blame<CR>",
            description = "Git line blame",
          },
          {
            "<leader>gs",
            "<cmd>Git<CR>",
            description = "Git status",
          },

          -- NvimTree
          {
            "<leader>t",
            "<cmd>NvimTreeToggle<CR>",
            description = "Toggle file tree",
          },

          -- lsp
          {
            "<leader>gd",
            vim.lsp.buf.definition,
            description = "LSP go to definition",
          },
          {
            "<leader>ca",
            vim.lsp.buf.code_action,
            description = "LSP code action",
          },
          {
            "<leader>h",
            vim.lsp.buf.hover,
            description = "LSP information under cursor",
          },
          {
            "<leader>H",
            "<cmd>Trouble diagnostics open follow=false focus=false<CR>",
            description = "Toggle Trouble viewer - focusing on item under the cursor",
          },
          {
            "<leader>dt",
            "<cmd>Trouble diagnostics toggle follow=false focus=false<CR>",
            description = "Toggle Trouble viewer",
          },
          {
            "<leader>d[",
            vim.diagnostic.goto_prev,
            description = "LSP diagnostic jump prev",
          },
          {
            "<leader>d]",
            vim.diagnostic.goto_next,
            description = "LSP diagnostic jump next",
          },

          -- searching
          {
            "<leader>sc",
            "<cmd>nohlsearch<CR>",
            description = "Clear search highlights",
          },

          -- UI for writing
          {
            "<leader>wz",
            "<cmd>ZenMode<CR>",
            description = "Toggle Zen mode",
          },
          {
            "<leader>wt",
            "<cmd>Twilight<CR>",
            description = "Toggle Twilight mode",
          },

          -- Lazy
          {
            "<leader>p",
            "<cmd>Lazy<CR>",
            description = "Lazy plugin manager",
          },

          -- zk
          {
            "<leader>zn",
            function()
              require("zk.commands").get("ZkNew")({
                template = "slip.md",
                dir = vim.fn.expand("~/notes"),
              })
            end,
            description = "Create new ZK slip",
          },
          {
            "<leader>zi",
            function()
              require("zk.commands").get("ZkInsertLink")({
                sort = { "modified" },
                dir = vim.fn.expand("~/notes"),
              })
            end,
            description = "Insert note link",
          },
          {
            "<leader>zl",
            "<Cmd>ZkBacklinks<CR>",
            description = "Open backlinks to the note",
          },
          {
            "<leader>zo",
            function()
              require("zk.commands").get("ZkNotes")({
                sort = { "modified" },
                dir = vim.fn.expand("~/notes"),
              })
            end,
            description = "Open ZK notes",
          },
          {
            "<leader>dapa",
            description = "DAP attach",
            function()
              require("dap.utils").pick_process()
            end,
          },
          {
            "<leader>dapi",
            function()
              require("dapui").toggle()
            end,
            description = "Toggle DAP UI",
          },
          {
            "<leader>dape",
            function()
              require("dapui").eval()
            end,
            desc = "Evaluate Expression",
          },
        },
        -- load extensions
        extensions = {
          -- load keymaps and commands from nvim-tree.lua
          nvim_tree = true,
          -- load commands from smart-splits.nvim
          -- and create keymaps, see :h legendary-extensions-smart-splits.nvim
          smart_splits = {
            directions = { "h", "j", "k", "l" },
            mods = {
              move = "<C>",
              resize = "<M>",
            },
          },
          -- load commands from op.nvim
          op_nvim = true,
          -- load keymaps from diffview.nvim
          diffview = true,
        },
      })
    end,
  },
  {
    "kristijanhusak/vim-dadbod-ui",
    dependencies = { "tpope/vim-dadbod" },
  },
  { "imsnif/kdl.vim" }
}, {
  config = {
    auto_update = true,
  },
})
