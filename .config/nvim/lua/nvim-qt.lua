if vim.g.neovide then
  vim.o.guifont = "OverpassM Nerd Font:h13"

  -- make cursor in Neovide have particles
  vim.g.neovide_cursor_vfx_mode = "wireframe"

  -- dynamic size scaling
  vim.g.neovide_scale_factor = 1.0
  local change_scale_factor = function(delta)
    vim.g.neovide_scale_factor = vim.g.neovide_scale_factor * delta
  end
  vim.keymap.set("n", "<C-=>", function()
    change_scale_factor(1.10)
  end)
  vim.keymap.set("n", "<C-->", function()
    change_scale_factor(1 / 1.10)
  end)
end
