local conditions = require("heirline.conditions")

local circle = " ⚬ "
-- doesn't work as I wish it would, so I have to keep it in sync :/
local colors = require("kanagawa.colors").setup({ theme = "wave" }).palette
local theme = require("kanagawa.colors").setup({ theme = "wave" }).theme

local ViMode = {
  init = function(self)
    self.mode = vim.fn.mode(2)
    if not self.once then
      vim.api.nvim_create_autocmd("ModeChanged", {
        pattern = "*:*o",
        command = "redrawstatus",
      })
      self.once = true
    end
  end,
  static = {
    mode_colors = {
      n = colors.springGreen,
      i = colors.lightBlue,
      v = colors.oniViolet,
      V = colors.oniViolet,
      ["\22"] = colors.sumiInk4,
      c = colors.lotusOrange,
      s = colors.carpYellow,
      S = colors.carpYellow,
      ["\19"] = colors.waveRed,
      r = colors.lotusAqua,
      R = colors.lotusAqua,
      ["!"] = colors.waveRed,
      t = colors.waveRed,
    },
    mode_labels = {
      n = "NORMAL",
      no = "N?",
      nov = "N?",
      noV = "N?",
      ["no\22"] = "N?",
      niI = "Ni",
      niR = "Nr",
      niV = "Nv",
      nt = "Nt",
      v = "V",
      vs = "Vs",
      V = "V_",
      Vs = "Vs",
      ["\22"] = "^V",
      ["\22s"] = "^V",
      s = "S",
      S = "S_",
      ["\19"] = "^S",
      i = "I",
      ic = "Ic",
      ix = "Ix",
      R = "R",
      Rc = "Rc",
      Rx = "Rx",
      Rv = "Rv",
      Rvc = "Rv",
      Rvx = "Rv",
      c = "C",
      cv = "Ex",
      r = "...",
      rm = "M",
      ["r?"] = "?",
      ["!"] = "!",
      t = "T",
    },
  },
  provider = function()
    if vim.bo.modified then
      return "    • "
    end

    return "    ◦ "
  end,
  hl = function(self)
    if not conditions.is_active() then
      return { bg = colors.sumiInk1, fg = colors.sumiInk6 }
    end

    local mode = self.mode:sub(1, 1)
    return { bg = self.mode_colors[mode], fg = colors.sumiInk1 }
  end,
  -- update = {
  --   "ModeChanged",
  --   "MenuPopup",
  --   "CmdlineEnter",
  --   "CmdlineLeave",
  -- },
}

local Filename = {
  init = function(self)
    self.filename = vim.api.nvim_buf_get_name(0)
  end,
  provider = function(self)
    return " " .. vim.fn.fnamemodify(self.filename, ":t") .. " "
  end,
  hl = {
    bold = true,
  },
}

local BlankFilename = {
  Filename,
  hl = {
    fg = colors.sumiInk2,
    bg = colors.sumiInk4,
  },
}

local ColoredFilename = {
  Filename,
  hl = {
    fg = colors.lightBlue,
  },
}

local FileFlags = {
  {
    provider = function()
      if not vim.bo.modifiable or vim.bo.readonly then
        return circle
      end
    end,
    hl = { bg = colors.dragonOrange },
  },
}

local Git = {
  condition = function()
    return conditions.is_git_repo() and not vim.bo.readonly
  end,

  init = function(self)
    self.status_dict = vim.b.gitsigns_status_dict
    self.has_changes = self.status_dict.added ~= 0 or self.status_dict.removed ~= 0 or self.status_dict.changed ~= 0
  end,

  hl = { fg = colors.oldWhite, bg = colors.sumiInk6 },

  {
    -- git branch name
    provider = function(self)
      local function truncate_string(str, length)
        if #str > length then
          return string.sub(str, 1, length) .. "…"
          -- Extract a substring from index 1 to 'length'
        else
          return str
          -- Return the original string if its length is less than or equal to the desired length
        end
      end

      return "  " .. truncate_string(self.status_dict.head, 10) .. " "
    end,
  },
  -- You could handle delimiters, icons and counts similar to Diagnostics
  {
    provider = function(self)
      local count = self.status_dict.added or 0
      return count > 0 and ("+" .. count .. " ")
    end,
    hl = { fg = theme.vcs.added },
  },
  {
    provider = function(self)
      local count = self.status_dict.removed or 0
      return count > 0 and ("-" .. count .. " ")
    end,
    hl = { fg = theme.vcs.removed },
  },
  {
    provider = function(self)
      local count = self.status_dict.changed or 0
      return count > 0 and ("~" .. count .. " ")
    end,
    hl = { fg = theme.vcs.changed },
  },
}

local CharacterColumn = {
  provider = function()
    return " %4(%c%) "
  end,
  hl = { fg = theme.ui.fg_dim, bg = theme.ui.bg_dim },
}

local Diagnostics = {
  condition = conditions.lsp_attached,

  init = function(self)
    self.errors = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.ERROR })
    self.warnings = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.WARN })
    self.hints = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.HINT })
    self.info = #vim.diagnostic.get(0, { severity = vim.diagnostic.severity.INFO })
  end,

  update = { "DiagnosticChanged", "BufEnter" },

  {
    provider = function(self)
      -- 0 is just another output, we can decide to print it or not!
      return self.errors > 0 and (circle .. self.errors .. " ")
    end,
    hl = { fg = colors.waveRed },
  },
  {
    provider = function(self)
      return self.warnings > 0 and (circle .. self.warnings .. " ")
    end,
    hl = { fg = colors.lotusOrange },
  },
  {
    provider = function(self)
      return self.info > 0 and (circle .. self.info .. " ")
    end,
    hl = { fg = colors.springBlue },
  },
  {
    provider = function(self)
      return self.hints > 0 and (circle .. self.hints .. " ")
    end,
    hl = { fg = colors.fujiGray },
  },
}

local Align = { provider = "%=" }

require("heirline").setup({
  statusline = {
    {
      condition = function()
        return conditions.buffer_matches({ bufname = { "NvimTree" } })
      end,
      Align,
      {
        provider = function()
          return " ⩀ tree "
        end,
        colors = {
          bg = colors.sumiInk1,
          fg = colors.sumiInk6,
        },
      },
    },
    {
      condition = function()
        return not conditions.buffer_matches({ bufname = { "NvimTree" } })
      end,
      ViMode,
      {
        condition = function()
          return conditions.is_active()
        end,
        ColoredFilename,
      },
      {
        condition = function()
          return not conditions.is_active()
        end,
        BlankFilename,
      },
      {
        condition = conditions.is_active,
        FileFlags,
        Align,
        Diagnostics,
        CharacterColumn,
        Git,
      },
    },
  },
  opts = { colors = colors },
})
