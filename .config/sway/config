set $mod Mod4
set $left h
set $down j
set $up k
set $right l
set $term kitty
    
set $launcher wofi --show drun | xargs swaymsg exec --
set $passmenu wofi --show drun | xargs swaymsg exec --
set $session  wofi --show drun | xargs swaymsg exec --

set $workspace_1    '일'
set $workspace_2    '이'
set $workspace_3    '삼'
set $workspace_4    '사'
set $workspace_5    '오'
set $workspace_6    '육'
set $workspace_7    '칠'
set $workspace_8    '팔'
set $workspace_9    '구'
set $workspace_10   '십'
    
## Base16 Tomorrow Night
# Author: Chris Kempson (http://chriskempson.com)
# set $base00 #1d1f21
# set $base01 #282a2e
# set $base02 #373b41
# set $base03 #969896
# set $base04 #b4b7b4
# set $base05 #c5c8c6
# set $base06 #e0e0e0
# set $base07 #ffffff
# set $base08 #cc6666
# set $base09 #de935f
# set $base0A #f0c674
# set $base0B #b5bd68
# set $base0C #8abeb7
# set $base0D #81a2be
# set $base0E #b294bb
# set $base0F #a3685a

## Kanagawa color scheme
set $base00 #1F1F28
set $base01 #2A2A37
set $base02 #223249
set $base03 #727169
set $base04 #C8C093
set $base05 #DCD7BA
set $base06 #938AA9
set $base07 #363646
set $base08 #C34043
set $base09 #FFA066
set $base0A #DCA561
set $base0B #98BB6C
set $base0C #7FB4CA
set $base0D #7E9CD8
set $base0E #957FB8
set $base0F #D27E99
    
# Basic color configuration using the Base16 variables for windows and borders.
# Property Name         Border  BG      Text    Indicator Child Border
client.focused          $base09 $base0D $base00 $base0A $base09
client.focused_inactive $base01 $base01 $base05 $base01 $base01
client.unfocused        $base01 $base00 $base05 $base01 $base01
client.urgent           $base08 $base08 $base00 $base08 $base08
client.placeholder      $base00 $base00 $base05 $base00 $base00
client.background       $base07

gaps inner 16
gaps top 6
default_border pixel 4
smart_borders no

input * xkb_layout pl,us

output * bg /home/kosciak/.config/secrets/wallpapers/kanagawa.png tile

output 'LG Electronics LG HDR 4K 0x00001B12' mode 3840x2160@60Hz scale 2 pos 0 0 transform 270
output "AOC Q27G2WG4 0x000009A0" mode 2560x1440@144Hz scale 1.33333 pos 1080 400 scale_filter nearest
# output "Iiyama North America PL2488H 0x00000101" mode 1920x1080@75Hz pos 3000 400

set $screenshot grim -g "$(slurp)" ~/screenshots/$(date +'%Y-%m-%d-%H%M%S_grim.png')

bindsym $mod+Return exec $term
bindsym $mod+space exec $launcher
bindsym $mod+Ctrl+space exec $passmenu

bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'
bindsym $mod+Shift+c reload

floating_modifier $mod normal
bindsym $mod+Shift+n floating toggle
bindsym $mod+Shift+space focus mode_toggle

bindsym $mod+b splith
bindsym $mod+v splitv

bindsym $mod+s layout stacking
bindsym $mod+Tab layout tabbed
bindsym $mod+e layout toggle split
bindsym $mod+a focus parent

bindsym $mod+1 workspace $workspace_1
bindsym $mod+2 workspace $workspace_2
bindsym $mod+3 workspace $workspace_3
bindsym $mod+4 workspace $workspace_4
bindsym $mod+5 workspace $workspace_5
bindsym $mod+6 workspace $workspace_6
bindsym $mod+7 workspace $workspace_7
bindsym $mod+8 workspace $workspace_8
bindsym $mod+9 workspace $workspace_9
bindsym $mod+0 workspace $workspace_10

bindsym $mod+Shift+1 move container to workspace $workspace_1
bindsym $mod+Shift+2 move container to workspace $workspace_2
bindsym $mod+Shift+3 move container to workspace $workspace_3
bindsym $mod+Shift+4 move container to workspace $workspace_4
bindsym $mod+Shift+5 move container to workspace $workspace_5
bindsym $mod+Shift+6 move container to workspace $workspace_6
bindsym $mod+Shift+7 move container to workspace $workspace_7
bindsym $mod+Shift+8 move container to workspace $workspace_8
bindsym $mod+Shift+9 move container to workspace $workspace_9
bindsym $mod+Shift+0 move container to workspace $workspace_10
        
bindsym $mod+Shift+minus move scratchpad
bindsym $mod+minus scratchpad show

bindsym $mod+w kill

bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right
        
bindsym $mod+f fullscreen
        
mode "resize" {
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

exec dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway
exec mako
exec dex
exec kdeconnect-indicator
exec redshift-gtk
exec swayidle -w timeout 300 'swaylock' \
       timeout 600 'swaymsg "output * dpms off"'\
       resume 'swaymsg "output * dpms on"' \
       before-sleep 'swaylock'
       
exec_always /home/kosciak/projects/.bin/import-gsettings \
    gtk-theme:gtk-theme-name \
    font-name:gtk-font-name \
    icon-theme:gtk-icon-theme-name \
    cursor-theme:gtk-cursor-theme-name
    
bar {
    swaybar_command waybar
}

include /etc/sway/config.d/*
